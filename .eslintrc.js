module.exports = {
  extends: ["eslint:recommended", "vue"],
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      impliedStrict: true
    }
  },
  env: {
    browser: true,
    commonjs: true,
    jasmine: true,
    node: true
  },
  globals: {
    "Modernizr": false
  },
  plugins: [
    "html"
  ],
  rules: {
    // allow debugger during development
    "curly": ["error", "all"],
    "dot-location": ["error", "property"],
    "eqeqeq": ["error", "smart"],
    "indent": ["error", 2, {
      "ArrayExpression": "first",
      "ObjectExpression": "first",
      "SwitchCase": 1
    }],
    "object-curly-spacing": ["error", "always"],
    "no-console": process.env.NODE_ENV === "production" ? 2 : 0,
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    "no-unused-vars": ["warn", "all"],
    "operator-linebreak": ["error", "before"],
    "padded-blocks": ["error", "never"],
    "quotes": ["error", "double"],
    "semi": ["error", "always"],
    "space-before-function-paren": ["error", "never"]
  }
};
