import Vue from "vue";
import VueI18n from "vue-i18n";
import en from "./en.json";
import zhTw from "./zh-TW.json";

Vue.use(VueI18n);

export function createI18n() {
  return new VueI18n({
    locale: "zh-TW",
    messages: {
      "en": en,
      "zh-TW": zhTw
    }
  });
}
