import axios from "axios";
import feathers from "feathers/client";
import rest from "feathers-rest/client";
import hooks from "feathers-hooks";
import auth from "feathers-authentication-client";
import feathersVuex from "feathers-vuex";
import store from "root/store/";

const restClient = rest(process.env.API_URL)
  .axios(axios);

const feathersClient = feathers()
  .configure(hooks())
  .configure(restClient)
  .configure(auth({ storage: window.localStorage }))
  .configure(feathersVuex(store, {
    idField: "_id"
  }));

feathersClient.service("/sentences");
feathersClient.service("/speech");

export default feathersClient;
