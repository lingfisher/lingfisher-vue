import Keen from "keen-tracking";
import MobileDetect from "mobile-detect";
import Compatibility from "root/compatibility";

export default class Analytics {
  constructor() {
    this.keen = new Keen({
      projectId: process.env.KEEN_PROJECT_ID,
      writeKey: process.env.KEEN_WRITE_KEY
    });

    const mobileDetect = new MobileDetect(window.navigator.userAgent);

    const sessionCookie = Keen.utils.cookie("lingfisher-alpha-4");
    if (!sessionCookie.get("guest_id")) {
      sessionCookie.set("guest_id", Keen.helpers.getUniqueId());
    }

    this.keen.extendEvents(() => {
      return {
        geo: {
          ipAddress: "${keen.ip}",
          info: {}
        },
        page: {
          title: document.title,
          url: document.location.href,
          info: {}
        },
        referrer: {
          url: document.referrer,
          info: {}
        },
        tech: {
          browser: Keen.helpers.getBrowserProfile(),
          voiceInput: Compatibility.canRecordAudio(),
          deviceType: mobileDetect.tablet() ? "tablet" : mobileDetect.mobile() ? "mobile" : "desktop",
          userAgent: "${keen.user_agent}",
          info: {}
        },
        time: Keen.helpers.getDatetimeIndex(),
        visitor: {
          guestId: sessionCookie.get("guest_id")
        },
        keen: {
          addons: [
            {
              name: "keen:ip_to_geo",
              input: {
                ip: "geo.ipAddress"
              },
              output: "geo.info"
            },
            {
              name: "keen:ua_parser",
              input: {
                ua_string: "tech.userAgent"  // eslint-disable-line camelcase
              },
              output: "tech.info"
            },
            {
              name: "keen:url_parser",
              input: {
                url: "page.url"
              },
              output: "page.info"
            },
            {
              name: "keen:referrer_parser",
              input: {
                referrer_url: "referrer.url",  // eslint-disable-line camelcase
                page_url: "page.url"  // eslint-disable-line camelcase
              },
              output: "referrer.info"
            }
          ]
        }
      };
    });
  }

  recordEvent(eventName, eventData) {
    return new Promise((resolve, reject) => {
      this.keen.recordEvent(eventName, eventData, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }

  listenTo(selector, callback) {
    const selectorEvents = {};
    selectorEvents[selector] = callback;
    Keen.listenTo(selectorEvents);
  }

  getGuestId() {
    const sessionCookie = Keen.utils.cookie("lingfisher-alpha-4");
    return sessionCookie.get("guest_id");
  }
}
