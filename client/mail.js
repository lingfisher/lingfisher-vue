import axios from "axios";
import Analytics from "root/analytics";

export default class Mail {
  constructor() {
    this.api = axios.create({
      baseURL: "https://api.mailerlite.com/api/v2",
      headers: {
        "Content-Type": "application/json",
        "X-MailerLite-ApiKey": process.env.MAILERLITE_API_KEY
      }
    });
  }

  addSubscriber(email, comment) {
    new Analytics().recordEvent("signup", { email, comment });

    // until mailerlite gets their act together we'll have to add emails manually
    return new Promise((resolve) => {
      resolve();
    });

    // what it SHOULD do:
    // return axios({
    //   url: "https://api.mailerlite.com/api/v2/groups/7562679/subscribers",
    //   method: "post",
    //   data: { email },
    //   headers: {
    //     "Content-Type": "application/json",
    //     "X-MailerLite-ApiKey": process.env.MAILERLITE_API_KEY
    //   }
    // });
  }

  static validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
