import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  audioFormat: null,
  isMicrophonePermitted: true,
  isMicrophoneAvailable: true,
  studySession: []
};

const getters = {
  isMicrophoneOn(state) {
    return state.isMicrophonePermitted && state.isMicrophoneAvailable;
  },

  currentCardIndex(state) {
    return state.studySession.findIndex(card => !card.complete);
  },

  sessionScore(state) {
    if (state.studySession && state.studySession.length) {
      const result = state.studySession.map(card => card.score)
        .filter(score => Number)
        .reduce((a, b) => a + b, 0) / state.studySession.filter(card => card.complete).length;

      return Number.isNaN(result) ? null : result;
    }
    return null;
  },

  sessionProgress(state, getters) {
    if (state.studySession && state.studySession.length) {
      return getters.currentCardIndex / state.studySession.length;
    }
    return null;
  }
};

const mutations = {
  ADD_TO_STUDY_SESSION(state, payload) {
    state.studySession.push(payload);
  },

  MARK_CARD_COMPLETE(state, payload) {
    const currentIndex = state.studySession.findIndex(card => card.sentence._id === payload.id);
    if (currentIndex < 0) {
      console.error(`Did not find card with id ${payload.id} in studySession.`);
      return;
    }
    const currentCard = state.studySession[currentIndex];
    Vue.set(state.studySession, currentIndex, { ...currentCard, complete: true, score: payload.score });
  },

  SET_MICROPHONE_PERMITTED(state) {
    state.isMicrophonePermitted = true;
  },

  SET_MICROPHONE_UNAVAILABLE(state) {
    state.isMicrophoneAvailable = false;
  },

  SET_MICROPHONE_UNPERMITTED(state) {
    state.isMicrophonePermitted = false;
  },

  SET_AUDIO_FORMAT(state, payload) {
    state.audioFormat = payload;
  }
};

const actions = {
  addToStudySession({ commit }, sentence) {
    commit("ADD_TO_STUDY_SESSION", { sentence, complete: false, score: null });
  },

  markCardComplete({ commit }, { id, score }) {
    commit("MARK_CARD_COMPLETE", { id, score });
  },

  setAudioFormat({ commit }, format) {
    commit("SET_AUDIO_FORMAT", format);
  },

  setMicrophonePermitted({ commit }) {
    commit("SET_MICROPHONE_PERMITTED");
  },

  setMicrophoneUnavailable({ commit }) {
    commit("SET_MICROPHONE_UNAVAILABLE");
  },

  setMicrophoneUnpermitted({ commit }) {
    commit("SET_MICROPHONE_UNPERMITTED");
  }
};

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});

export default store;
