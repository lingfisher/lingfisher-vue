export default class Compatibility {
  static canRecordAudio() {
    return typeof MediaRecorder !== "undefined";
  }

  static canPlayOgg() {
    return Modernizr.audio.ogg;
  }

  static canPlayMp3() {
    return Modernizr.audio.mp3;
  }
}
