import Vue from "vue";
import { sync } from "vuex-router-sync";
import App from "./components/App";
import router from "./router";
import store from "./store";
import { createI18n } from "./i18n";
import "root/api/feathers-client";

sync(store, router);

const app = new Vue({
  router,
  store,
  i18n: createI18n(),
  ...App
});

export { app, router, store };
