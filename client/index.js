import "./promise-polyfill";
import adapter from  "webrtc-adapter";
import { app } from "./app";

window.adapter = adapter;

// Enable progressive web app support (with offline-plugin)
if (process.env.NODE_ENV === "production") {
  require("./pwa");
}

app.$mount("#app");
