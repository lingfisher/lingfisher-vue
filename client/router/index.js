import Vue from "vue";
import Router from "vue-router";

import Home from "../views/Home";
import Tutorial from "../views/Tutorial";
import Review from "../views/Review";
import ReviewItem from "../views/ReviewItem";
import Advertisement from "../views/Advertisement";
import Signup from "../views/Signup";
import ThankYou from "../views/ThankYou";

Vue.use(Router);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/tutorial",
    name: "tutorial",
    component: Tutorial
  },
  {
    path: "/review",
    name: "review",
    component: Review,
    children: [
      {
        path: ":id",
        name: "review-item",
        component: ReviewItem
      }
    ]
  },
  {
    path: "/ad",
    name: "ad",
    component: Advertisement
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup
  },
  {
    path: "/thank-you",
    name: "thank-you",
    component: ThankYou
  }
];

export default new Router({
  mode: "history",
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    }
    return { y: 0 };
  },
  linkActiveClass: "is-active",
  routes
});
