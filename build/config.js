'use strict'
const pkg = require('../package')

module.exports = {
  title: 'Lingfisher - 忙人的英語救星',
  // Options for webpack-dev-server
  // See https://webpack.js.org/configuration/dev-server
  devServer: {
    host: 'localhost',
    port: 4000,
    historyApiFallback: {
      index: "build/index.html",
      htmlAcceptHeaders: ['text/html', 'application/json']
    }
  },
  // when you use electron please set to relative path like ./
  // otherwise only set to absolute path when you're using history mode
  publicPath: '/'
}
