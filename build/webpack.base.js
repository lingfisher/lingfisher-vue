"use strict";
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ModernizrPlugin = require("modernizr-webpack-plugin");
const config = require("./config");
const _ = require("./utils");

module.exports = {
  entry: {
    client: "./client/index.js"
  },
  output: {
    path: _.outputPath,
    filename: "[name].js",
    publicPath: config.publicPath,
    // Point sourcemap entries to original disk location
    devtoolModuleFilenameTemplate: info => path.resolve(info.absoluteResourcePath),
    // Add /* filename */ comments to generated require()s in the output.
    pathinfo: true
  },
  performance: {
    hints: process.env.NODE_ENV === "production" ? "warning" : false
  },
  resolve: {
    extensions: [".js", ".vue", ".css", ".json"],
    alias: {
      root: path.join(__dirname, "../client"),
      components: path.join(__dirname, "../client/components"),
      styles: path.join(__dirname, "../client/styles")
    },
    modules: [
      _.cwd("node_modules"),
      // this meanse you can get rid of dot hell
      // for example import "components/Foo" instead of import "../../components/Foo"
      _.cwd("client")
    ]
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          loaders: {
            "scss": "vue-style-loader!css-loader!sass-loader",
            "sass": "vue-style-loader!css-loader!sass-loader?indentedSyntax"
          }
        }
      },
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: "file-loader",
        options: {
          name: "[name].[ext]?[hash]"
        }
      },
      {
        test: /\.(mp3|aac)$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          name: "[name].[hash:7].[ext]"
        }
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 10000,
              mimetype: "application/font-woff"
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          { loader: "file-loader" }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: config.title,
      template: path.resolve(__dirname, "index.html"),
      filename: _.outputIndexPath
    }),
    new ModernizrPlugin({
      options: [
        "setClasses"
      ],
      filename: "modernizr.js",
      noChunk: true,
      "feature-detects": [
        "test/audio"
      ]
    }),
    new webpack.LoaderOptionsPlugin(_.loadersOptions()),
    new CopyWebpackPlugin([
      {
        from: _.cwd("./static"),
        // to the roor of dist path
        to: "./"
      }
    ])
  ],
  target: _.target
};
