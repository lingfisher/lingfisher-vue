const path = require("path");
const express = require("express");
const cors = require("cors");
const serveStatic = require("serve-static");
const history = require("connect-history-api-fallback");

const app = express();

app.use(cors({ origin: true }));
app.use(history());
app.use(serveStatic(__dirname + "/dist"));

app.listen(process.env.PORT || 5000);
